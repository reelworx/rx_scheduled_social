<?php

$EM_CONF[$_EXTKEY] = [
	'title' => 'Scheduled Social',
	'description' => 'Post news articles to social media platforms and fetch likes from your social media account',
	'category' => 'plugin',
	'version' => '3.0.0',
	'state' => 'stable',
	'uploadfolder' => 0,
	'author' => 'Johannes Kasberger',
	'author_email' => 'office@reelworx.at',
	'author_company' => 'Reelworx GmbH',
	'constraints' => [
		'depends' => [
		    'php' => '7.4.0-8.1.99',
			'typo3' => '10.4.99-11.5.99',
            'scheduler' => '10.4.99-11.5.99',
            'news' => '9.0.0-10.99.99',
        ],
		'conflicts' => [],
		'suggests' => [],
    ],
];
