<?php
defined('TYPO3_MODE') || die('Access denied.');

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Reelworx\RxScheduledSocial\Scheduler\TwitterPublishTask::class] = [
    'extension' => 'rx_scheduled_social',
    'title' => 'Publish news to Twitter',
    'description' => 'Publish new news posts to Twitter',
    'additionalFields' => \Reelworx\RxScheduledSocial\Scheduler\TwitterPublishAdditionalFieldsProvider::class
];

$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][\Reelworx\RxScheduledSocial\Scheduler\TwitterFetchFollowersTask::class] = [
    'extension' => 'rx_scheduled_social',
    'title' => 'Fetch follower count of twitter user',
    'description' => 'Fetches the follower count of a twitter user',
    'additionalFields' => \Reelworx\RxScheduledSocial\Scheduler\TwitterFetchFollowersAdditionalFieldsProvider::class
];
