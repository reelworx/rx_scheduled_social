﻿.. include:: ../Includes.txt


Change Log
==========

Version 3.0.0
-------------

* Dropped support for Facebook
* Dropped support for TYPO3 < 10
* Dropped non-composer installation support
* Updated twitter library
* PHP 8.1 compatibility


Version 2.1.5
-------------

* Complete refactoring of fake Frontend generation for CLI link crafting


Version 2.1.4
-------------

* Fixed link generation for twitter publishing with TYPO3 v10


Version 2.1.3
-------------

* Updated change log


Version 2.1.2
-------------

* Fixed ext_locallang.php compatibility
* Precise PHP version requirement definition
* Minor code improvements


Version 2.1.1
-------------

* Fixed invalid composer.json


Version 2.1.0
-------------

* Added TYPO3 v10 LTS support


Version 2.0.6
-------------

* Fix composer.json file


Version 2.0.5
-------------

* More bugfixes for hidden breaking changes in TYPO3 v9


Version 2.0.4
-------------

* Bugfix release


Version 2.0.3
-------------

* Bugfix release


Version 2.0.2
-------------

* Bugfix release


Version 2.0.1
-------------

* Bugfix release


Version 2.0.0
-------------

* Added support for TYPO3 version 9.5
* Dropped support for TYPO3 version 7.6
* Removed Facebook publishing capabilities
* Improved documentation


Version 1.0.1
-------------

Allow TYPO3 version 8.7 and news version up to 7


Version 1.0.0
-------------

Initial release
