﻿.. include:: ../Includes.txt


Support and Contributing
========================


Known issues
------------

None, at the moment.


Translations
------------

Reelworx maintains the English and German translations, all other translations are very welcome.


Support
-------

In case of errors check the TYPO3 log files for more details.

You can also use this snippet in your site packages's `ext_localconf.php` to have the log entries
of this extension in a dedicated log file:

.. code-block:: php

   $GLOBALS['TYPO3_CONF_VARS']['LOG']['Reelworx']['RxScheduledSocial']['writerConfiguration'][
      \TYPO3\CMS\Core\Log\LogLevel::NOTICE => [
         \TYPO3\CMS\Core\Log\Writer\FileWriter::class => [
            'logFile' => 'typo3temp/var/logs/rx_scheduled_social.log'
         ],
      ],
   ];


Paid support is provided by Reelworx. Please get in touch with us: support@reelworx.at
