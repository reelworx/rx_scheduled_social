﻿.. include:: ../Includes.txt


.. _user-manual:

User manual
===========

First off: This extension does not provide any plugins, but works entirely in the background.

If the "fetch likes/follower" tasks are used, a Fluid view helper may be used to output acquired statistics in the frontend.
The id then specifies the screen name (twitter) that has been configured in the scheduler task, which fetches the numbers.

In the following example a button for twitter is generated.

.. code-block:: html

  <html xmlns:f="http://typo3.org/ns/TYPO3/CMS/Fluid/ViewHelpers"
        xmlns:rx="http://typo3.org/ns/Reelworx/RxScheduledSocial/ViewHelpers"
        data-namespace-typo3-fluid="true"
  >
    <a class="btn btn-t" href="https://twitter.com/<youraccount>" target="_blank">
      <i class="fab fa-twitter" aria-hidden="true"></i>
      <span class="hidden-xs">Follow @<youraccount></span>
      <span class="badge">
        <aw:socialStatistics id="<youraccount>" />
      </span>
    </a>
  </html>

.. image:: ../Images/UserManual/btns.png
