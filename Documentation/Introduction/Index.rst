﻿.. include:: ../Includes.txt


What does it do?
================

This extension provides scheduler tasks to communicate with social networks.
Currently the supported platform is Twitter.

The following task are provided:

- Tweet link to news post on Twitter
- Fetch follower count of a twitter user

The setup of the tasks is described in the :ref:`admin-manual`.

Publish news posts
------------------

The extension ensures that each news post is only published when the news post is active. A post is shared only once per platform.
The extension uses the **title**, the **teaser** and the **link** to the news post as content of the published item.


Fetching likes/follower count - GDPR compliant
----------------------------------------------

Usually a vendor specific Javascript framework is necessary to display the amount of likes a page currently has.
Such Javascript frameworks expose the visitor of the page to the social network and increases the amount data to load.
With the provided tasks it's possible to fetch the numbers by the server and use a Fluid view helper to display them.
This way no external Javascript frameworks are necessary and the visitor's privacy is improved.
