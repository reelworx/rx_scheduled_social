﻿.. include:: ../Includes.txt


.. _admin-manual:

Administrator Manual
====================


Installation
------------

Install the extension via the Extension Manager or composer as usual.


Configuration
-------------

The extension requires a working linkhandler configuration for ext:news. See example in the documentation of ext:news.


Accessing social networks
-------------------------

To access the API of the social networks, you need to retrieve access tokens.
The necessary steps are described below. The tokes are required in order to have working tasks.


Twitter
^^^^^^^

Steps to retrieve the necessary tokens to access Twitter:

#. Go to https://apps.twitter.com/.
#. Create a new app and fill in the necessary details.
#. On the app management page select the Tab "Keys and Access Tokens".
#. Press "Create my access tokens".
#. Copy the according values into the task configuration.

Scheduler Task Configuration
----------------------------

#. Select the Scheduler module and add a new task.
#. Select the class of the task (all available tasks are grouped under *rx_scheduled_social*).
#. Enter the scheduler information (run frequency/start/stop time). For more details read the :ref:`Scheduler documentation <scheduler:introduction>`.
#. Enter the login tokens, the necessary information depends on the site that is the target of the task.
#. Enter the identifier of the linkhandler configuration to use.
#. Enter task specific configuration options.


Publish News Task
^^^^^^^^^^^^^^^^^

.. container:: table-row

   Property
      News Detail PID

   Data type
      integer

   Description
      The page id with the news plugin that displays the detail page.

   Required / Example
      Yes / 1


.. container:: table-row

   Property
      News storage PID list

   Data type
      comma-separated list of uids

   Description
      If specified only news posts from the given storage ids (and children of the specified ids) are posted.

   Required / Example
      No / 5,6


.. container:: table-row

   Property
      Category ID list

   Data type
      comma-separated list of uids

   Description
      If specified only news posts from the given categories (and child categories of the specified ids) are posted.

   Required / Example
      No / 5,6


.. note::

  Sharing the same news post on two different accounts from the same platform is currently not supported

Fetch Followers Task
^^^^^^^^^^^^^^^^^^^^

.. container:: table-row

   Property
      Twitter Screen Name

   Data type
      string

   Description
      The screen name of the Twitter account.

   Required / Example
      Yes

