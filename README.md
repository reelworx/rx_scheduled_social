Scheduled Social
================

This extension provides two functionalities:
 - automatically publish news posts (ext:news) to Twitter
 - fetch number of likes of your Twitter account

Specifically fetching the number of likes is beneficial as no external JavaScript
solution is necessary. Therefore, it prevents worries regarding GDPR.

Technically this is done with the help of scheduler tasks.
For more information please consider the extension manual https://docs.typo3.org/p/reelworx/rx-scheduled-social/2.1/en-us/
