<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\ViewHelpers;

use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * @author Johannes Kasberger <johannes.kasberger@reelworx.at>
 */
class SocialStatisticsViewHelper extends AbstractViewHelper
{
    public function initializeArguments()
    {
        $this->registerArgument('id', 'string', 'screen name for twitter', true);
    }

    /**
     * @return string
     */
    public function render()
    {
        $id = trim((string)$this->arguments['id']);
        if (empty($id)) {
            return '';
        }

        $registry = GeneralUtility::makeInstance(Registry::class);
        return $registry->get('rx_scheduled_social', 'twitter_' . $id . '.follower_count');
    }
}
