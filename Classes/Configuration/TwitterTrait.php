<?php

declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Configuration;

trait TwitterTrait
{
    public string $appId = '';
    public string $appSecret = '';
    public string $appToken = '';
    public string $appTokenSecret = '';

    protected function isTwitterConfigurationValid(): bool
    {
        return !empty($this->appId) && !empty($this->appSecret) && !empty($this->appToken)
            && !empty($this->appTokenSecret);
    }

    public function getAppId(): string
    {
        return $this->appId;
    }

    public function setAppId(string $appId): void
    {
        $this->appId = $appId;
    }

    public function getAppSecret(): string
    {
        return $this->appSecret;
    }

    public function setAppSecret(string $appSecret): void
    {
        $this->appSecret = $appSecret;
    }

    public function getAppToken(): string
    {
        return $this->appToken;
    }

    public function setAppToken(string $appToken): void
    {
        $this->appToken = $appToken;
    }

    public function getAppTokenSecret(): string
    {
        return $this->appTokenSecret;
    }

    public function setAppTokenSecret(string $appTokenSecret): void
    {
        $this->appTokenSecret = $appTokenSecret;
    }
}
