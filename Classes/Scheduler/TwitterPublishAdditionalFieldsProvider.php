<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Scheduler;

use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Messaging\AbstractMessage;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Messaging\FlashMessageService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Page\PageRepository;
use TYPO3\CMS\Scheduler\AdditionalFieldProviderInterface;
use TYPO3\CMS\Scheduler\Controller\SchedulerModuleController;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

class TwitterPublishAdditionalFieldsProvider implements AdditionalFieldProviderInterface
{
    /**
     * gets additional fields
     *
     * @param array $taskInfo
     * @param TwitterPublishTask $task
     * @param SchedulerModuleController $schedulerModule
     * @return array A two dimensional array
     */
    public function getAdditionalFields(array &$taskInfo, $task, SchedulerModuleController $schedulerModule)
    {
        $additionalFields = [];

        $appId = '';
        $appSecret = '';
        $appToken = '';
        $appTokenSecret = '';
        $detailPid = 0;
        $pidList = '';
        $categoryList = '';
        $recordLinkId = '';

        if ($task) {
            $appId = $task->getAppId();
            $appSecret = $task->getAppSecret();
            $appToken = $task->getAppToken();
            $appTokenSecret = $task->getAppTokenSecret();
            $detailPid = $task->getDetailPid();
            $pidList = $task->getPidList();
            $categoryList = $task->getCategoryList();
            $recordLinkId = $task->getRecordLinkId();
        }

        $additionalFields['appId'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tp][appId]" value="' . htmlspecialchars($appId) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:twitter.appid',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['appSecret'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tp][appSecret]" value="' . htmlspecialchars($appSecret) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:twitter.appsecret',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['appToken'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tp][appToken]" value="' . htmlspecialchars($appToken) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:twitter.apptoken',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['appTokenSecret'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tp][appTokenSecret]" value="' . htmlspecialchars($appTokenSecret) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:twitter.apptokensecret',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['detailPid'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tp][detailPid]" value="' . $detailPid . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:detailpid',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['pidlist'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tp][pidlist]" value="' . htmlspecialchars($pidList) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:pidlist',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['categorylist'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tp][categorylist]" value="' . htmlspecialchars($categoryList) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:categorylist',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        $additionalFields['recordLinkId'] = [
            'code' => '<input class="form-control" type="text" name="tx_scheduler[tp][recordLinkId]" value="' . htmlspecialchars($recordLinkId) . '" />',
            'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:recordLinkId',
            'cshKey' => '',
            'cshLabel' => '',
        ];

        return $additionalFields;
    }

    /**
     * Validates the additional fields' values
     *
     * @param array $submittedData
     * @param SchedulerModuleController $schedulerModule
     * @return boolean TRUE if validation was ok, FALSE otherwise
     * @throws Exception
     */
    public function validateAdditionalFields(array &$submittedData, SchedulerModuleController $schedulerModule)
    {
        $errors = [];

        $data = $submittedData['tp'];

        if (trim((string)$data['appId']) === '') {
            $errors[] = 'scheduler.error.appid_isempty';
        }

        if (trim((string)$data['appSecret']) === '') {
            $errors[] = 'scheduler.error.appsecret_isempty';
        }

        if (trim((string)$data['appToken']) === '') {
            $errors[] = 'scheduler.error.apptoken_isempty';
        }

        if (trim((string)$data['appTokenSecret']) === '') {
            $errors[] = 'scheduler.error.apptokensecret_isempty';
        }

        if (trim((string)$data['recordLinkId']) === '') {
            $errors[] = 'scheduler.error.recordLinkId_isempty';
        }

        $detailPid = (int)$data['detailPid'];
        if ($detailPid <= 0) {
            $errors[] = 'scheduler.error.detailpid_invalid';
        } else {
            $pageRepository = GeneralUtility::makeInstance(PageRepository::class);
            $page = $pageRepository->getPage($detailPid);
            if (empty($page)) {
                $errors[] = 'scheduler.error.detailpid_invalid';
            }
        }

        $pattern = '#^(?: *[0-9]+ *,)* *[0-9]+ *$#';
        if (trim((string)$data['pidlist']) !== '' && preg_match($pattern, (string)$data['pidlist']) !== 1) {
            $errors[] = 'scheduler.error.pidlist_invalid';
        }

        if (trim((string)$data['categorylist']) !== '' && preg_match($pattern, (string)$data['categorylist']) !== 1) {
            $errors[] = 'scheduler.error.categorylist_invalid';
        }

        foreach ($errors as $error) {
            $error = $GLOBALS['LANG']->sL('LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:' . $error);
            $this->addErrorMessage($error);
        }

        return empty($errors);
    }

    /**
     * set values in the task object
     *
     * @param array $submittedData
     * @param AbstractTask $task
     * @return void
     */
    public function saveAdditionalFields(array $submittedData, AbstractTask $task)
    {
        $data = $submittedData['tp'];

        /** @var TwitterPublishTask $task */
        $task->setAppId((string)$data['appId']);
        $task->setAppSecret((string)$data['appSecret']);
        $task->setAppToken((string)$data['appToken']);
        $task->setAppTokenSecret((string)$data['appTokenSecret']);
        $task->setDetailPid((int)$data['detailPid']);
        $task->setPidList((string)$data['pidlist']);
        $task->setCategoryList((string)$data['categorylist']);
        $task->setRecordLinkId((string)$data['recordLinkId']);
    }

    /**
     * @param $message
     * @throws Exception
     */
    protected function addErrorMessage($message)
    {
        $flashMessage = GeneralUtility::makeInstance(FlashMessage::class, $message, '', AbstractMessage::ERROR);
        /** @var FlashMessage $flashMessage */
        $flashMessageService = GeneralUtility::makeInstance(FlashMessageService::class);
        /** @var FlashMessageService $flashMessageService */
        $flashMessageService->getMessageQueueByIdentifier()->enqueue($flashMessage);
    }
}
