<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Scheduler;

use Doctrine\DBAL\DBALException;
use Reelworx\TYPO3\FakeFrontend\FrontendUtility;
use TYPO3\CMS\Core\Database\Connection;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use GeorgRinger\News\Service\CategoryService;
use GeorgRinger\News\Utility\Page;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

abstract class AbstractNewsTask extends AbstractTask
{
    protected int $detailPid = 0;
    protected string $pidList = '';
    protected string $categoryList = '';
    protected string $recordLinkId = '';

    protected function isNewsConfigurationValid(): bool
    {
        return $this->detailPid !== 0;
    }

    protected function fetchNews(string $postedDbFlagName, callable $doPostAction): bool
    {
        $qb = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_news_domain_model_news');
        $qb->select('n.uid', 'n.teaser', 'n.title')
            ->from('tx_news_domain_model_news', 'n')
            ->where($qb->expr()->eq('n.' . $postedDbFlagName, 0));

        if (!empty($this->pidList)) {
            $pidList = Page::extendPidListByChildren($this->pidList, 5);
            if ($pidList !== '') {
                $qb->andWhere($qb->expr()->in('n.pid', $qb->createNamedParameter($pidList)));
            }
        }

        if (!empty($this->categoryList)) {
            $categories = CategoryService::getChildrenCategories($this->categoryList);
            if ($categories !== '') {
                $qb->join('n', 'sys_category_record_mm', 'c', $qb->expr()->eq('c.uid_foreign', 'n.uid'));
                $qb->andWhere($qb->expr()->eq('c.tablenames',
                    $qb->createNamedParameter('tx_news_domain_model_news')));
                $qb->andWhere($qb->expr()->in('n.uid_local', $qb->createNamedParameter($categories)));
            }
        }

        try {
            $res = $qb->execute();
        } catch (DBALException $e) {
            $this->logger->alert('Fetching of news posts failed with ' . $e->getCode());
            return false;
        }

        $requestBackup = $GLOBALS['TYPO3_REQUEST'] ?? null;
        if (!FrontendUtility::buildFakeFE($this->detailPid)) {
            $this->logger->alert('Building FakeFE failed', ['exception' => FrontendUtility::$lastError]);
            return false;
        }

        $hasErrors = false;
        $updateBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_news_domain_model_news');
        while ($row = $res->fetchAssociative()) {
            // use linkhandler to generate the link to the news post
            $linkConfig = [
                'parameter' => 't3://record?identifier='. $this->recordLinkId . '&uid=' . (int)$row['uid'],
                'forceAbsoluteUrl' => '1',
            ];

            $postData = [
                'link' => $GLOBALS['TSFE']->cObj->typoLink_URL($linkConfig),
                'message' => trim($row['teaser']),
                'title' => trim($row['title']),
            ];

            if ($postData['link'] !== '' && $doPostAction($postData)) {
                $updateBuilder->update('tx_news_domain_model_news')
                    ->set($postedDbFlagName, 1)
                    ->where($updateBuilder->expr()->eq('uid',
                        $updateBuilder->createNamedParameter((int)$row['uid'], Connection::PARAM_INT)));
                if (!$updateBuilder->execute()) {
                    $this->logger->alert('Update of ' . $row['uid'] . ' failed with ' . $updateBuilder->getConnection()->errorCode());
                }
            } else {
                $this->logger->alert('Publishing of news uid ' . $row['uid'] . ' failed', $postData);
                $hasErrors = true;
            }

        }
        $res->free();
        if ($requestBackup) {
            $GLOBALS['TYPO3_REQUEST'] = $requestBackup;
        }
        unset($GLOBALS['TSFE']);
        return !$hasErrors;
    }

    public function getDetailPid(): int
    {
        return $this->detailPid;
    }

    public function setDetailPid(int $detailPid)
    {
        $this->detailPid = $detailPid;
    }

    public function getPidList(): string
    {
        return $this->pidList;
    }

    public function setPidList(string $pidList)
    {
        $this->pidList = $pidList;
    }

    public function getCategoryList(): string
    {
        return $this->categoryList;
    }

    public function setCategoryList(string $categoryList)
    {
        $this->categoryList = $categoryList;
    }

    public function getRecordLinkId(): string
    {
        return $this->recordLinkId;
    }

    public function setRecordLinkId(string $recordLinkId): void
    {
        $this->recordLinkId = $recordLinkId;
    }
}
