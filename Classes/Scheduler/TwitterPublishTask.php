<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Scheduler;

use Abraham\TwitterOAuth\TwitterOAuth;
use Reelworx\RxScheduledSocial\Configuration\TwitterTrait;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class TwitterPublishTask extends AbstractNewsTask
{
    use TwitterTrait;

    public function execute()
    {
        if (!$this->isTwitterConfigurationValid() || !$this->isNewsConfigurationValid()) {
            $this->logger->error('Twitter publish setup is incomplete.');
            return false;
        }

        return $this->fetchNews('twitter_published', function ($postData) {
            $status = $postData['title'];
            // URL always counted as 22 strlen, 1 space
            $maxLength = 140 - 24;
            if (strlen($status) > $maxLength) {
                $status = GeneralUtility::fixed_lgd_cs($status, $maxLength - 3);
            } // -3 for dots that are appended

            $status .= ' ' . $postData['link'];

            $connection = new TwitterOAuth($this->appId, $this->appSecret, $this->appToken, $this->appTokenSecret);
            $statues = $connection->post('statuses/update', ['status' => $status]);

            if (!isset($statues->id)) {
                $this->logger->error('Twitter return an error: ' . print_r($statues, true));
                return false;
            }
            return true;
        });
    }
}
