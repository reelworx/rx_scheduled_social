<?php
declare(strict_types=1);

/*
 *
 * This file is part of the rx_scheduled_social Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * Copyright (c) Reelworx GmbH
 *
 */

namespace Reelworx\RxScheduledSocial\Scheduler;

use Abraham\TwitterOAuth\TwitterOAuth;
use Reelworx\RxScheduledSocial\Configuration\TwitterTrait;
use TYPO3\CMS\Core\Registry;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Scheduler\Task\AbstractTask;

class TwitterFetchFollowersTask extends AbstractTask
{
    use TwitterTrait;

    public string $screenName = '';

    public function execute()
    {
        $registry = GeneralUtility::makeInstance(Registry::class);

        if (!$this->isTwitterConfigurationValid() || empty($this->screenName)) {
            $this->logger->error('Twitter fetch follower setup is incomplete.');
            return false;
        }

        $connection = new TwitterOAuth($this->appId, $this->appSecret, $this->appToken, $this->appTokenSecret);
        $content = $connection->get('users/show', ['screen_name' => $this->screenName]);
        $count = $content->followers_count;

        $registry->set('rx_scheduled_social', 'twitter_' . $this->screenName . '.follower_count', $count);

        return true;
    }

    public function getScreenName(): string
    {
        return $this->screenName;
    }

    public function setScreenName(string $screenName)
    {
        $this->screenName = $screenName;
    }
}
