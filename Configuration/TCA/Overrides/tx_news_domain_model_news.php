<?php
defined('TYPO3_MODE') || die ('Access denied.');

$socialColumns = array (
    'twitter_published' => [
        'exclude' => 1,
        'label' => 'LLL:EXT:rx_scheduled_social/Resources/Private/Language/locallang.xlf:tx_news_domain_model_news.twitter_published',
        'config' => [
            'type' => 'check',
            'items' => [
                1 => [
                    'Yes'
                ]
            ]
        ]
    ],
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('tx_news_domain_model_news', $socialColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('tx_news_domain_model_news', 'twitter_published', '', 'after:alternative_title');
